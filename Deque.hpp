// -------
// Deque.h
// --------

#ifndef Deque_h
#define Deque_h

// --------
// includes
// --------

#include <algorithm>        // copy, equal, lexicographical_compare, max, swap
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iterator>         // bidirectional_iterator_tag
#include <memory>           // allocator
#include <stdexcept>        // out_of_range
#include <utility>          // !=, <=, >, >=

// -----
// using
// -----
using std::rel_ops::operator!=;
using std::rel_ops::operator<=;
using std::rel_ops::operator>;
using std::rel_ops::operator>=;

// -------
// destroy
// -------

template <typename A, typename BI>
BI my_destroy(A &a, BI b, BI e)
{
    while (b != e)
    {
        --e;
        a.destroy(&*e);
    }
    return b;
}

// ------------------
// uninitialized_copy
// ------------------

template <typename A, typename II, typename BI>
BI my_uninitialized_copy(A &a, II b, II e, BI x)
{
    BI p = x;
    try
    {
        while (b != e)
        {
            a.construct(&*x, *b);
            ++b;
            ++x;
        }
    }
    catch (...)
    {
        my_destroy(a, p, x);
        throw;
    }
    return x;
}

// ------------------
// uninitialized_fill
// ------------------

template <typename A, typename BI, typename T>
void my_uninitialized_fill(A &a, BI b, BI e, const T &v)
{
    BI p = b;
    try
    {
        while (b != e)
        {
            a.construct(&*b, v);
            ++b;
        }
    }
    catch (...)
    {
        my_destroy(a, p, b);
        throw;
    }
}

// --------
// my_deque
// --------

template <typename T, typename A = std::allocator<T>>
class my_deque
{
    // -----------
    // operator ==
    // -----------

    /**
     * @param a reference to a constant my_deque lhs, a reference to a constant my_deque rhs
     * @return true if lhs and rhs are equal, false otherwise
     * criteria: the same number of elements and each element in lhs compares equal with the element in rhs at the same position
     */
    friend bool operator==(const my_deque &lhs, const my_deque &rhs)
    {
        if (lhs.size() != rhs.size())
        {
            return false;
        }
        return std::equal(lhs.begin(), lhs.end(), rhs.begin());
    }

    // ----------
    // operator <
    // ----------

    /**
     * @param a reference to a constant my_deque lhs, a reference to a constant my_deque rhs
     * @return true if the contents of lhs is "smaller then" the contents of rhs lexicographically, false otherwise
     */
    friend bool operator<(const my_deque &lhs, const my_deque &rhs)
    {
        size_type smaller_size = lhs.size();
        if (smaller_size > rhs.size())
        {
            smaller_size = rhs.size();
        }
        // get the smaller size, and compare te two deques up to smaller_size
        // if equal up to smaller_size, lhs < rhs is only true when lhs is shorter than rhs
        if (std::equal(lhs.begin(), lhs.begin() + smaller_size, rhs.begin()))
        {
            return (lhs.size() < rhs.size());
        }
        // not equal up to smaller_size, return the result of lexicographical_compare
        return std::lexicographical_compare(lhs.begin(), lhs.begin() + smaller_size, rhs.begin(), rhs.begin() + smaller_size);
        ;
    }

    // ----
    // swap
    // ----

    /**
     * @param a reference to a my_deque x, a reference to a my_deque y
     * @brief exchanges the contents of x and y
     * does not invoke any move, copy, or swap operations on individual elements
     */
    friend void swap(my_deque &x, my_deque &y)
    {
        x.swap(y);
    }

public:
    // ------
    // usings
    // ------

    // you must use this allocator for the inner arrays
    using allocator_type = A;
    using value_type = typename allocator_type::value_type;

    using size_type = typename allocator_type::size_type;
    using difference_type = typename allocator_type::difference_type;

    using pointer = typename allocator_type::pointer;
    using const_pointer = typename allocator_type::const_pointer;

    using reference = typename allocator_type::reference;
    using const_reference = typename allocator_type::const_reference;

    // you must use this allocator for the outer array
    using allocator_type_2 = typename A::template rebind<pointer>::other;

private:
    // ----
    // data
    // ----

    allocator_type _a_inner;   // inner
    allocator_type_2 _a_outer; // outer

    T *_b_size = nullptr;                 // pointer to first T
    T *_e_size = nullptr;                 // pointer to last T + 1
    T **_begin_populated_inner = nullptr; // pointer to the address of first populated array
    T **_end_populated_inner = nullptr;   // pointer to the address of last populated array + 1

    size_type _s = 0;
    size_type inner_size = 10;

private:
    // -----
    // valid
    // -----

    bool valid() const
    {
        if (_s == 0)
        {
            // everything unassigned
            if (_begin_populated_inner == nullptr)
            {
                return true;
            }

            // we still allocate one inner array in preparation for future growth in the case of _s = 0 || shrinked to _begin_populated_inner == _end_populated_inner
            return ((_begin_populated_inner == _end_populated_inner - 1) && (_b_size == _e_size)) || (_begin_populated_inner == _end_populated_inner);
        }
        // cnt: count how many Ts are actually in the deque
        // goal: test if there's any empty spots (hole) inside the deque
        //       make sure the validity of _b_size, _e_size, _begin_populated_inner, _end_populated_inner
        size_type cnt = 0;
        // if there is only one inner array
        if (_begin_populated_inner == _end_populated_inner - 1)
        {
            cnt += _e_size - _b_size;
        }
        else
            // there are multiple inner arrays
        {
            // add the number of elements in the first inner array, might be full or not full
            cnt += (*_begin_populated_inner + inner_size) - _b_size;
            // add the number of elements in the second to second last arrays, all must be full
            cnt += (_end_populated_inner - _begin_populated_inner - 2) * inner_size;
            // add the number of elements in the last inner array, might be full or not full
            cnt += _e_size - *(_end_populated_inner - 1);
        }
        return cnt == _s;
    }

public:
    // --------
    // iterator
    // --------

    class iterator
    {

        // -----------
        // operator ==
        // -----------

        /**
         * @param a reference to a const iterator, a reference to a const iterator
         * @return true if the two iterator objects are equal in terms of contents, false otherwise
         */
        friend bool operator==(const iterator &lhs, const iterator &rhs)
        {
            return (lhs.container == rhs.container) && (lhs.index == rhs.index);
        }

        /**
         * @param a reference to a const iterator, a reference to a const iterator
         * @return true if the two iterator objects are NOT equal in terms of contents, false otherwise
         */
        friend bool operator!=(const iterator &lhs, const iterator &rhs)
        {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * @param an iterator lhs, a difference_type rhs
         * @brief moves the iterator forward by rhs
         * @return the updated iterator
         */
        friend iterator operator+(iterator lhs, difference_type rhs)
        {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * @param an iterator lhs, a difference_type rhs
         * @brief moves the iterator backward by rhs
         * @return the updated iterator
         */
        friend iterator operator-(iterator lhs, difference_type rhs)
        {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = typename my_deque::value_type;
        using difference_type = typename my_deque::difference_type;
        using pointer = typename my_deque::pointer;
        using reference = typename my_deque::reference;

    private:
        // ----
        // data
        // ----

        my_deque<T> *container;
        size_type index;

    private:
        // -----
        // valid
        // -----

        bool valid() const
        {
            // compare with negative one to prevent assertion error after --index for the last time.
            return (0 <= index && index <= container->size()) || ((int)index == -1);
        }

    public:
        // --------
        // getIndex
        // --------

        /**
         * @return the current index the iterator is pointing at
         */
        size_type getIndex() const
        {
            assert(valid());
            return index;
        }

        // -----------
        // constructor
        // -----------

        /**
         * @brief constructor of iterator
         */
        iterator(my_deque &d, size_type start_idx)
            : container(&d) // initializing container with &d
        {
            index = start_idx;
            assert(valid());
        }

        iterator(const iterator &) = default;
        ~iterator() = default;
        iterator &operator=(const iterator &) = default;

        // ----------
        // operator *
        // ----------

        /**
         * @return a reference to the element pointed to by the iterator
         */
        reference operator*() const
        {
            assert(valid());
            return (*container)[index];
        }

        // -----------
        // operator ->
        // -----------

        /**
         * @brief arrow operator
         * @return an address of this iterator
         */
        pointer operator->() const
        {
            assert(valid());
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * @brief pre-incrementation
         * @return a reference to the updated iterator
         */
        iterator &operator++()
        {
            ++index;
            assert(valid());
            return *this;
        }

        /**
         * @brief post-incrementation
         * @return an iterator pointing at the original position
         */
        iterator operator++(int)
        {
            iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * @brief pre-decrementation
         * @return a reference to the updated iterator
         */
        iterator &operator--()
        {
            --index;
            assert(valid());
            return *this;
        }

        /**
         * @brief post-decrementation
         * @return an iterator pointing at the original position
         */
        iterator operator--(int)
        {
            iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * @param a "d" of difference_type
         * @brief moves this iterator forward by d
         * @return a reference to this updated iterator
         */
        iterator &operator+=(difference_type d)
        {
            index += d;
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * @param a "d" of difference_type
         * @brief moves the iterator backward by d
         * @return a reference to this updated iterator
         */
        iterator &operator-=(difference_type d)
        {
            index -= d;
            assert(valid());
            return *this;
        }
    };

public:
    // --------------
    // const_iterator
    // --------------

    class const_iterator
    {

        // -----------
        // operator ==
        // -----------

        /**
         * @param a reference to a const const_iterator, a reference to a const const_iterator
         * @return true if the two iterator objects are equal in terms of contents, false otherwise
         */
        friend bool operator==(const const_iterator &lhs, const const_iterator &rhs)
        {
            return (lhs.container == rhs.container) && (lhs.index == rhs.index);
        }

        /**
         * @param a reference to a const const_iterator, a reference to a const const_iterator
         * @return true if the two iterator objects are NOT equal in terms of contents, false otherwise
         */
        friend bool operator!=(const const_iterator &lhs, const const_iterator &rhs)
        {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * @param a const_iterator lhs, a difference_type rhs
         * @brief moves the iterator forward by rhs
         * @return the updated const_iterator
         */
        friend const_iterator operator+(const_iterator lhs, difference_type rhs)
        {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * @param a const_iterator lhs, a difference_type rhs
         * @brief moves the iterator backward by rhs
         * @return the updated const_iterator
         */
        friend const_iterator operator-(const_iterator lhs, difference_type rhs)
        {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = typename my_deque::value_type;
        using difference_type = typename my_deque::difference_type;
        using pointer = typename my_deque::const_pointer;
        using reference = typename my_deque::const_reference;

    private:
        // ----
        // data
        // ----

        const my_deque<T> *container;
        size_type index;

    private:
        // -----
        // valid
        // -----

        bool valid() const
        {
            // || compare with negative one to prevent assertion error after --index for the last time
            return (0 <= index && index <= container->size()) || ((int)index == -1);
        }

    public:
        // --------
        // getIndex
        // --------

        /**
         * @return the current index the iterator is pointing at
         */
        size_type getIndex() const
        {
            assert(valid());
            return index;
        }

        // -----------
        // constructor
        // -----------

        /**
         * @brief contructor of const_iterator
         * methods of const_iterator would be called if the calling object is a const my_deque
         */
        const_iterator(const my_deque &d, difference_type start_idx)
            : container(&d) // initialize the container with &d
        {
            index = start_idx;
            assert(valid());
        }

        const_iterator(const const_iterator &) = default;
        ~const_iterator() = default;
        const_iterator &operator=(const const_iterator &) = default;

        // ----------
        // operator *
        // ----------

        /**
         * @return a reference to the element pointed to by the const_iterator
         */
        reference operator*() const
        {
            assert(valid());
            return (*container)[index];
        }

        // -----------
        // operator ->
        // -----------

        /**
         * @brief arrow operator
         * @return an address of this const_iterator
         */
        pointer operator->() const
        {
            assert(valid());
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * @brief pre-incrementation
         * @return a reference to the updated const_iterator
         */
        const_iterator &operator++()
        {
            ++index;
            assert(valid());
            return *this;
        }

        /**
         * @brief post-incrementation
         * @return a const_iterator pointing at the original position
         */
        const_iterator operator++(int)
        {
            const_iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * @brief pre-decrementation
         * @return a reference to the updated const_iterator
         */
        const_iterator &operator--()
        {
            --index;
            assert(valid());
            return *this;
        }

        /**
         * @brief post-decrementation
         * @return a const_iterator pointing at the original position
         */
        const_iterator operator--(int)
        {
            const_iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * @param a "d" of difference_type
         * @brief moves this const_iterator forward by d
         * @return a reference to this updated const_iterator
         */
        const_iterator &operator+=(difference_type d)
        {
            index += d;
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * @param a "d" of difference_type
         * @brief moves the const_iterator backward by d
         * @return a reference to this updated const_iterator
         */
        const_iterator &operator-=(difference_type d)
        {
            index -= d;
            assert(valid());
            return *this;
        }
    };

public:
    // ------------
    // constructors
    // reference: https://en.cppreference.com/w/cpp/container/deque/deque
    // ------------

    my_deque() = default;

    /**
     * explicit constructor
     * @param an "s" of size_type
     * @brief constructs the container with space reserved for s default-inserted instances of T
     */
    explicit my_deque(size_type s)
    {
        _s = s;

        // less than 10 spaces to be reserved
        if (_s <= inner_size)
        {
            _begin_populated_inner = _a_outer.allocate(1);
            _end_populated_inner = _begin_populated_inner + 1;
            *_begin_populated_inner = _a_inner.allocate(inner_size);
            _b_size = (*_begin_populated_inner) + (inner_size - _s) / 2;

            my_uninitialized_fill(_a_inner, _b_size, _b_size + _s, 0);
            _e_size = _b_size + _s;
            assert(valid());
            return;
        }

        // more than 10 spaces to be reserved, do calculations to see how many inner arrays are needed
        size_type num_ptrs_to_inner = _s / inner_size + 2;
        size_type num_inner_left = _s % inner_size;

        // Example 1                              1*          2*
        // [..........] [0000000000] [0000000000] [..........]  // 20
        //               ^                      ^
        //            _b_size                _e_size

        // Example 2
        // [.........0] [0000000000] [0000000000] [0.........]  // 22
        //           ^                              ^
        //        _b_size                        _e_size

        _begin_populated_inner = _a_outer.allocate(num_ptrs_to_inner);
        _end_populated_inner = _begin_populated_inner + num_ptrs_to_inner;

        *_begin_populated_inner = _a_inner.allocate(inner_size);

        // put default elements into first populated inner array and set _b_size
        T *var = *_begin_populated_inner + inner_size - (num_inner_left / 2);
        _b_size = var;
        for (size_type i = 0; i < (num_inner_left / 2); ++i)
        {
            *var = 0;
            ++var;
        }

        // put default elements into the full inner arrays "in the middle"
        for (T **index_outer = _begin_populated_inner + 1; index_outer != _end_populated_inner - 1; ++index_outer)
        {
            // (*index_outer) is the pointer to the inner array, space allocated below
            *index_outer = _a_inner.allocate(inner_size);
            my_uninitialized_fill(_a_inner, *index_outer, (*index_outer) + inner_size, 0);
        }

        // put default elements into the last populated array and set _e_size
        *(_end_populated_inner - 1) = _a_inner.allocate(inner_size);
        for (size_type i = 0; i < num_inner_left - (num_inner_left / 2); ++i)
        {
            (*(_end_populated_inner - 1))[i] = 0;
            _e_size = (*(_end_populated_inner - 1)) + i + 1;
        }

        // in this case, the one extra default element is placed in the last populated inner array instead of the first
        // we didn't enter the first loop where _b_size would be set, so we need to set it here
        if (num_inner_left == 1)
        {
            ++_begin_populated_inner;
            _b_size = *_begin_populated_inner;
        }

        // in this case, we don't have any extra elements and we didn't enter the first and last loops
        // where we set _b_size and _e_size, so we have to alter _begin_populated_inner and _end_populated_inner(from 2* to 1* in Example 1)
        // and set _b_size and _e_size here
        if (!num_inner_left)
        {
            ++_begin_populated_inner;
            --_end_populated_inner;
            _b_size = *_begin_populated_inner;
            _e_size = *(_end_populated_inner - 1) + inner_size;
        }

        assert(valid());
    }

    /**
         * @param an "s" of size_type, a reference to const v (const_reference)
         * @brief constructs the container with s instances of v
         */
    my_deque(size_type s, const_reference v)
    {
        _s = s;

        // less than 10 spaces to be reserved
        if (_s <= inner_size)
        {
            _begin_populated_inner = _a_outer.allocate(1);
            _end_populated_inner = _begin_populated_inner + 1;
            *_begin_populated_inner = _a_inner.allocate(inner_size);
            _b_size = (*_begin_populated_inner) + (inner_size - _s) / 2;

            my_uninitialized_fill(_a_inner, _b_size, _b_size + _s, v);
            _e_size = _b_size + _s;
            assert(valid());
            return;
        }

        // more than 10 spaces to be reserved, do calculations to see how many inner arrays are needed
        size_type num_ptrs_to_inner = _s / inner_size + 2;
        size_type num_inner_left = _s % inner_size;

        // Example 1                              1*          2*
        // [..........] [0000000000] [0000000000] [..........] // 20
        //               ^                      ^
        //            _b_size                _e_size

        // Example 2
        // [.........0] [0000000000] [0000000000] [0.........] // 22
        //           ^                              ^
        //        _b_size                        _e_size

        _begin_populated_inner = _a_outer.allocate(num_ptrs_to_inner);
        _end_populated_inner = _begin_populated_inner + num_ptrs_to_inner;

        // allocate space for first inner array and put "v"s into first populated inner array and set _b_size
        *_begin_populated_inner = _a_inner.allocate(inner_size);
        T *var = *_begin_populated_inner + inner_size - (num_inner_left / 2);
        _b_size = var;
        for (size_type i = 0; i < (num_inner_left / 2); ++i)
        {
            *var = v;
            ++var;
        }

        // put "v"s into the full inner arrays "in the middle"
        for (T **index_outer = _begin_populated_inner + 1; index_outer != _end_populated_inner - 1; ++index_outer)
        {
            // (*index_outer) is the pointer to the inner array, space allocated on the next line
            *index_outer = _a_inner.allocate(inner_size);
            my_uninitialized_fill(_a_inner, *index_outer, (*index_outer) + inner_size, v);

        }

        // put "v"s into the last populated array and set _e_size
        *(_end_populated_inner - 1) = _a_inner.allocate(inner_size);
        for (size_type i = 0; i < num_inner_left - (num_inner_left / 2); ++i)
        {
            (*(_end_populated_inner - 1))[i] = v;
            _e_size = (*(_end_populated_inner - 1)) + i + 1;
        }

        // in this case, the one extra "v" is placed in the last populated inner array instead of the first
        // we didn't enter the first loop where _b_size would be set, so we need to set it here
        if (num_inner_left == 1)
        {
            ++_begin_populated_inner;
            _b_size = *_begin_populated_inner;
        }

        // in this case, we don't have any extra elements and we didn't enter the first and last loops
        // where we set _b_size and _e_size, so we have to alter _begin_populated_inner and _end_populated_inner(from 2* to 1* in Example 1)
        // and set _b_size and _e_size here
        if (!num_inner_left)
        {
            ++_begin_populated_inner;
            --_end_populated_inner;
            _b_size = *_begin_populated_inner;
            _e_size = *(_end_populated_inner - 1) + inner_size;
        }

        assert(valid());
    }

    /**
     * @param an "s" of size_type, a reference to const v (const_reference), a reference to a const allocator a
     * @brief constructs the container with s instances of v using allocator a
     */
    my_deque(size_type s, const_reference v, const allocator_type &a)
    {
        _s = s;
        _a_inner = a;

        // less than 10 spaces to be reserved
        if (_s <= inner_size)
        {
            _begin_populated_inner = _a_outer.allocate(1);
            _end_populated_inner = _begin_populated_inner + 1;
            *_begin_populated_inner = _a_inner.allocate(inner_size);
            _b_size = (*_begin_populated_inner) + (inner_size - _s) / 2;

            // put "v"s in the middle of the only inner array
            my_uninitialized_fill(_a_inner, _b_size, _b_size + _s, v);
            _e_size = _b_size + _s;
            assert(valid());
            return;
        }

        // more than 10 spaces to be reserved, do calculations to see how many inner arrays are needed
        size_type num_ptrs_to_inner = _s / inner_size + 2;
        size_type num_inner_left = _s % inner_size;

        // Example 1                              1*          2*
        // [..........] [0000000000] [0000000000] [..........] // 20
        //               ^                      ^
        //            _b_size                _e_size

        // Example 2
        // [.........0] [0000000000] [0000000000] [0.........] // 22
        //           ^                              ^
        //        _b_size                        _e_size

        _begin_populated_inner = _a_outer.allocate(num_ptrs_to_inner);
        _end_populated_inner = _begin_populated_inner + num_ptrs_to_inner;

        // allocate spaces for first inner array and put "v"s into first populated inner array and set _b_size
        *_begin_populated_inner = _a_inner.allocate(inner_size);
        T *var = *_begin_populated_inner + inner_size - (num_inner_left / 2);
        _b_size = var;
        for (size_type i = 0; i < (num_inner_left / 2); ++i)
        {
            *var = v;
            ++var;
        }

        // put "v"s into the full inner arrays "in the middle"
        for (T **index_outer = _begin_populated_inner + 1; index_outer != _end_populated_inner - 1; ++index_outer)
        {
            // (*index_outer) is the pointer to the inner array, space allocated on the next line
            *index_outer = _a_inner.allocate(inner_size);
            my_uninitialized_fill(_a_inner, *index_outer, (*index_outer) + inner_size, v);

        }

        // allocate spaces for the last populated inner array
        *(_end_populated_inner - 1) = _a_inner.allocate(inner_size);
        // put "v"s into the last populated array and set _e_size
        for (size_type i = 0; i < num_inner_left - (num_inner_left / 2); ++i)
        {
            (*(_end_populated_inner - 1))[i] = v;
            _e_size = (*(_end_populated_inner - 1)) + i + 1;
        }

        // in this case, the one extra "v" is placed in the last populated inner array instead of the first
        // we didn't enter the first loop where _b_size would be set, so we need to set it here
        if (num_inner_left == 1)
        {
            ++_begin_populated_inner;
            _b_size = *_begin_populated_inner;
        }

        // in this case, we don't have any extra elements and we didn't enter the first and last loops
        // where we set _b_size and _e_size, so we have to alter _begin_populated_inner and _end_populated_inner(from 2* to 1* in Example 1)
        // and set _b_size and _e_size here
        if (!num_inner_left)
        {
            ++_begin_populated_inner;
            --_end_populated_inner;
            _b_size = *_begin_populated_inner;
            _e_size = *(_end_populated_inner - 1) + inner_size;
        }

        assert(valid());
    }

    /**
     * @param an initializer_list of value_type values
     * @brief constructs the container with the contents of the initializer_list
     */
    my_deque(std::initializer_list<value_type> rhs)
    {
        _s = rhs.size();
        // iterate over the initializer_list and put them into my_deque
        const_pointer ptr = rhs.begin();

        // less than 10 spaces to be reserved
        if (_s <= inner_size)
        {
            _begin_populated_inner = _a_outer.allocate(1);
            _end_populated_inner = _begin_populated_inner + 1;
            *_begin_populated_inner = _a_inner.allocate(inner_size);
            _b_size = (*_begin_populated_inner) + (inner_size - _s) / 2;

            my_uninitialized_copy(_a_inner, ptr, ptr + _s, _b_size);
            _e_size = _b_size + _s;
            assert(valid());
            return;
        }

        // more than 10 spaces to be reserved, do calculations to see how many inner arrays are needed
        size_type num_ptrs_to_inner = _s / inner_size + 2;
        size_type num_inner_left = _s % inner_size;

        // Example 1                              1*          2*
        // [..........] [0000000000] [0000000000] [..........] // 20
        //               ^                      ^
        //            _b_size                _e_size

        // Example 2
        // [.........0] [0000000000] [0000000000] [0.........] // 22
        //           ^                              ^
        //        _b_size                        _e_size

        _begin_populated_inner = _a_outer.allocate(num_ptrs_to_inner);
        _end_populated_inner = _begin_populated_inner + num_ptrs_to_inner;

        // allocate spaces for first inner array and put elements into first populated inner array
        *_begin_populated_inner = _a_inner.allocate(inner_size);
        T *var = *_begin_populated_inner + inner_size - (num_inner_left / 2);
        // set _b_size
        _b_size = var;
        for (size_type i = 0; i < (num_inner_left / 2); ++i)
        {
            *var = *ptr;
            ++var;
            ++ptr;
        }

        // put elements into the full inner arrays "in the middle"
        for (T **index_outer = _begin_populated_inner + 1; index_outer != _end_populated_inner - 1; ++index_outer)
        {
            // (*index_outer) is the pointer to the inner array, space allocated on the next line
            *index_outer = _a_inner.allocate(inner_size);
            my_uninitialized_copy(_a_inner, ptr, ptr + inner_size, *index_outer);
            ptr += inner_size;
        }

        // allocate spaces for the last populated inner array and populate it
        *(_end_populated_inner - 1) = _a_inner.allocate(inner_size);
        for (size_type i = 0; i < num_inner_left - (num_inner_left / 2); ++i)
        {
            (*(_end_populated_inner - 1))[i] = *ptr;
            _e_size = (*(_end_populated_inner - 1)) + i + 1;
            ++ptr;
        }

        // in this case, the one extra element is placed in the last populated inner array instead of the first
        // we didn't enter the first loop where _b_size would be set, so we need to set it here
        if (num_inner_left == 1)
        {
            ++_begin_populated_inner;
            _b_size = *_begin_populated_inner;
        }

        // in this case, we don't have any extra elements and we didn't enter the first and last loops
        // where we set _b_size and _e_size, so we have to alter _begin_populated_inner and _end_populated_inner(from 2* to 1* in Example 1)
        // and set _b_size and _e_size here
        if (!num_inner_left)
        {
            ++_begin_populated_inner;
            --_end_populated_inner;
            _b_size = *_begin_populated_inner;
            _e_size = *(_end_populated_inner - 1) + inner_size;
        }
        assert(valid());
    }

    /**
     * @param an initializer_list of value_type values, a reference to a const allocator
     * @brief constructs the container with the contents of the initializer_list using given allocator a
     */
    my_deque(std::initializer_list<value_type> rhs, const allocator_type &a)
    {
        _s = rhs.size();
        _a_inner = a;
        // iterate through rhs and put elements into my_deque
        const_pointer ptr = rhs.begin();

        // less than 10 spaces to be reserved
        if (_s <= inner_size)
        {
            _begin_populated_inner = _a_outer.allocate(1);
            _end_populated_inner = _begin_populated_inner + 1;
            *_begin_populated_inner = _a_inner.allocate(inner_size);
            _b_size = (*_begin_populated_inner) + (inner_size - _s) / 2;

            my_uninitialized_copy(_a_inner, ptr, ptr + _s, _b_size);
            _e_size = _b_size + _s;
            assert(valid());
            return;
        }

        // more than 10 spaces to be reserved, do calculations to see how many inner arrays are needed
        size_type num_ptrs_to_inner = _s / inner_size + 2;
        size_type num_inner_left = _s % inner_size;

        // Example 1                              1*          2*
        // [..........] [0000000000] [0000000000] [..........] // 20
        //               ^                      ^
        //            _b_size                _e_size

        // Example 2
        // [.........0] [0000000000] [0000000000] [0.........] // 22
        //           ^                              ^
        //        _b_size                        _e_size

        _begin_populated_inner = _a_outer.allocate(num_ptrs_to_inner);
        _end_populated_inner = _begin_populated_inner + num_ptrs_to_inner;

        // allocate spaces for first inner array and put elements into first populated inner array
        *_begin_populated_inner = _a_inner.allocate(inner_size);
        T *var = *_begin_populated_inner + inner_size - (num_inner_left / 2);
        // set _b_size
        _b_size = var;
        for (size_type i = 0; i < (num_inner_left / 2); ++i)
        {
            *var = *ptr;
            ++var;
            ++ptr;
        }

        // put elements into the full inner arrays "in the middle"
        for (T **index_outer = _begin_populated_inner + 1; index_outer != _end_populated_inner - 1; ++index_outer)
        {
            // (*index_outer) is the pointer to the inner array, space allocated on the next line
            *index_outer = _a_inner.allocate(inner_size);
            my_uninitialized_copy(_a_inner, ptr, ptr + inner_size, *index_outer);
            ptr += inner_size;
        }

        // allocate spaces for the last populated inner array and populate it
        *(_end_populated_inner - 1) = _a_inner.allocate(inner_size);
        for (size_type i = 0; i < num_inner_left - (num_inner_left / 2); ++i)
        {
            (*(_end_populated_inner - 1))[i] = *ptr;
            _e_size = (*(_end_populated_inner - 1)) + i + 1;
            ++ptr;
        }

        // in this case, the one extra element is placed in the last populated inner array instead of the first
        // we didn't enter the first loop where _b_size would be set, so we need to set it here
        if (num_inner_left == 1)
        {
            ++_begin_populated_inner;
            _b_size = *_begin_populated_inner;
        }

        // in this case, we don't have any extra elements and we didn't enter the first and last loops
        // where we set _b_size and _e_size, so we have to alter _begin_populated_inner and _end_populated_inner(from 2* to 1* in Example 1)
        // and set _b_size and _e_size here
        if (!num_inner_left)
        {
            ++_begin_populated_inner;
            --_end_populated_inner;
            _b_size = *_begin_populated_inner;
            _e_size = *(_end_populated_inner - 1) + inner_size;
        }

        assert(valid());
    }

    /**
         * copy constructor
         * @param a reference to a const my_deque, "that"
         * @brief constructs the container with the copy of the contents of other
         */
    my_deque(const my_deque &that)
    {
        _s = that.size();
        // iterate through that and put elements into this my_deque
        const_iterator ptr = that.begin();

        // less than 10 spaces to be reserved
        if (_s <= inner_size)
        {
            _begin_populated_inner = _a_outer.allocate(1);
            _end_populated_inner = _begin_populated_inner + 1;
            *_begin_populated_inner = _a_inner.allocate(inner_size);
            _b_size = (*_begin_populated_inner) + (inner_size - _s) / 2;

            my_uninitialized_copy(_a_inner, ptr, ptr + _s, _b_size);
            _e_size = _b_size + _s;
            assert(valid());
            return;
        }

        // more than 10 spaces to be reserved, do calculations to see how many inner arrays are needed
        size_type num_ptrs_to_inner = _s / inner_size + 2;
        size_type num_inner_left = _s % inner_size;

        // Example 1                              1*          2*
        // [..........] [0000000000] [0000000000] [..........] // 20
        //               ^                      ^
        //            _b_size                _e_size

        // Example 2
        // [.........0] [0000000000] [0000000000] [0.........] // 22
        //           ^                              ^
        //        _b_size                        _e_size

        _begin_populated_inner = _a_outer.allocate(num_ptrs_to_inner);
        _end_populated_inner = _begin_populated_inner + num_ptrs_to_inner;

        // allocate spaces for first inner array and put elements into first populated inner array
        *_begin_populated_inner = _a_inner.allocate(inner_size);
        T *var = *_begin_populated_inner + inner_size - (num_inner_left / 2);
        // set _b_size
        _b_size = var;
        for (size_type i = 0; i < (num_inner_left / 2); ++i)
        {
            *var = *ptr;
            ++var;
            ++ptr;
        }

        // put elements into the full inner arrays "in the middle"
        for (T **index_outer = _begin_populated_inner + 1; index_outer != _end_populated_inner - 1; ++index_outer)
        {
            // (*index_outer) is the pointer to the inner array, space allocated on the next line
            *index_outer = _a_inner.allocate(inner_size);
            my_uninitialized_copy(_a_inner, ptr, ptr + inner_size, *index_outer);
            ptr += inner_size;
        }

        // allocate spaces for the last populated inner array and populate it
        *(_end_populated_inner - 1) = _a_inner.allocate(inner_size);
        for (size_type i = 0; i < num_inner_left - (num_inner_left / 2); ++i)
        {
            (*(_end_populated_inner - 1))[i] = *ptr;
            _e_size = (*(_end_populated_inner - 1)) + i + 1;
            ++ptr;
        }

        // in this case, the one extra element is placed in the last populated inner array instead of the first
        // we didn't enter the first loop where _b_size would be set, so we need to set it here
        if (num_inner_left == 1)
        {
            ++_begin_populated_inner;
            _b_size = *_begin_populated_inner;
        }

        // in this case, we don't have any extra elements and we didn't enter the first and last loops
        // where we set _b_size and _e_size, so we have to alter _begin_populated_inner and _end_populated_inner(from 2* to 1* in Example 1)
        // and set _b_size and _e_size here
        if (!num_inner_left)
        {
            ++_begin_populated_inner;
            --_end_populated_inner;
            _b_size = *_begin_populated_inner;
            _e_size = *(_end_populated_inner - 1) + inner_size;
        }

        assert(valid());
    }

    // ----------
    // destructor
    // ----------

    /**
     * @brief destructs the deque
     * the destructors of the elements called, the used storage deallocated
     * if the elements are pointers, the pointed-to objects are not destroyed
     */
    ~my_deque()
    {
        my_destroy(_a_outer, _begin_populated_inner, _end_populated_inner);
    }

    // ----------
    // operator =
    // ----------

    /**
     * @brief copy assignment operator, replaces the contents with a copy of the contents of rhs
     * @param a reference to a constant my_deque called rhs
     * @return a reference to the updated my_deque(*this)
     */
    my_deque &operator=(const my_deque &rhs)
    {
        // use copy constructor and swap to perform the assignment
        my_deque tmp(rhs);
        swap(tmp);

        assert(valid());
        return *this;
    }

    // -----------
    // operator []
    // -----------

    /**
     * @param an index of size_type
     * @return a reference to the element at specified index
     * no bounds checking performed
     */
    reference operator[](size_type index)
    {
        // guard against the case where the user calls the default constructor to make the deque
        // in which case, some instance variables are undefined and would later cause errors
        if (_s == 0)
        {
            my_deque tmp(0);
            swap(tmp);
        }

        T **pointer = _begin_populated_inner;
        size_type num_elements_in_first_inner = inner_size - (_b_size - *_begin_populated_inner);
        // if target element is in the first inner array
        if (index < num_elements_in_first_inner)
        {
            assert(valid());
            return *(_b_size + index);
        }

        ++pointer;

        // find the element, constant time effort
        index -= num_elements_in_first_inner;
        pointer += index / inner_size;
        index %= inner_size;
        assert(valid());
        return *(*pointer + index);
    }

    /**
     * @param an index of size_type
     * @return a const_reference to the element at specified index
     * no bounds checking performed
     */
    const_reference operator[](size_type index) const
    {
        assert(valid());
        return const_cast<my_deque *>(this)->operator[](index);
    }

    // --
    // at
    // --

    /**
     * @param an index of size_type
     * @return a reference to the element at specified index, with bounds checking
     * @throws out_of_range if index not within the range of the container
     */
    reference at(size_type index)
    {
        if (index < 0 || index >= _s)
        {
            throw std::out_of_range("index not within the range of the container");
        }
        assert(valid());
        return (*this)[index];
    }

    /**
     * @param an index of size_type
     * @return a const_reference to the element at specified index, with bounds checking
     * @throws out_of_range if index not within the range of the container
     */
    const_reference at(size_type index) const
    {
        if (index < 0 || index >= _s)
        {
            throw std::out_of_range("index not within the range of the container");
        }
        assert(valid());
        return const_cast<my_deque *>(this)->at(index);
    }

    // ----
    // back
    // ----

    /**
     * @return reference to the last element in the container
     * calling back on an empty container is undefined
     * for a non-empty container c, this is equivalent to *std::prev(c.end())
     */
    reference back()
    {
        // guard against the case where the user calls the default constructor to make the deque
        // in which case, some instance variables are undefined and would later cause errors
        if (_s == 0)
        {
            my_deque tmp(0);
            swap(tmp);
        }
        assert(valid());
        return *(_e_size - 1);
    }

    /**
     * @return const_reference to the last element in the container
     * calling back on an empty container is undefined
     * for a non-empty container c, this is equivalent to *std::prev(c.end())
     */
    const_reference back() const
    {
        assert(valid());
        return const_cast<my_deque *>(this)->back();
    }

    // -----
    // begin
    // -----

    /**
     * @return an iterator to the first element of the deque
     * if the deque is empty, the returned iterator will be equal to end()
     */
    iterator begin()
    {
        assert(valid());
        return iterator(*this, 0);
    }

    /**
     * @return a const_iterator to the first element of the deque
     * if the deque is empty, the returned iterator will be equal to end()
     */
    const_iterator begin() const
    {
        assert(valid());
        return const_iterator(*this, 0);
    }

    // -----
    // clear
    // -----

    /**
     * @brief erase all elements from the container
     * invalidates any references, pointers, or iterators referring to contained elements
     * any pass-the-end iterators also invalidated
     * size() returns 0 after this call
     */
    void clear()
    {
        // guard against the case where the user calls the default constructor to make the deque
        // in which case, some instance variables are undefined and would later cause errors
        if (_s == 0)
        {
            my_deque tmp(0);
            swap(tmp);
            return;
        }
        resize(0);
        assert(valid());
    }

    // -----
    // empty
    // -----

    /**
     * @brief checks if the container has no elements
     * @return true if the container is empty, false otherwise
     */
    bool empty() const
    {
        assert(valid());
        return !size();
    }

    // ---
    // end
    // ---

    /**
     * @return an iterator to the element following the last element of the deque
     */
    iterator end()
    {
        if (_s == 0)
        {
            my_deque tmp(0);
            swap(tmp);
        }
        assert(valid());
        return iterator(*this, _s);
    }

    /**
     * @return a const_iterator to the element following the last element of the deque
     */
    const_iterator end() const
    {
        assert(valid());
        return const_iterator(*this, _s);
    }

    // -----
    // erase
    // -----

    /**
     * @param iterator to the element to remove
     * @brief removes the element pointed to by the iterator passed in
     * @return an iterator following the last removed element
     * if iterator passed in refers to the last element, then end() iterator is returned
     */
    iterator erase(iterator a)
    {
        if (_s == 0)
        {
            assert(valid());
            return a;
        }
        size_type idx = a.getIndex();
        if (idx == 0)
        {
            // time complexity: constant
            pop_front();
            assert(valid());
            return (*this).begin();
        }
        else if (idx == _s - 1)
        {
            // time complexity: constant
            pop_back();
            assert(valid());
            return (*this).end();
        }
        else
        {
            // time complexity: linear
            size_type num_elements_in_last_populated = _e_size - *(_end_populated_inner - 1);
            // shift all elements on the right of idx to the left by one
            while (idx < _s - 1)
            {
                (*this)[idx] = (*this)[idx + 1];
                ++idx;
            }
            --_s;

            // if there is only one element in the last populated array before the erase
            // we need to change _e_size and _end_populated_inner
            if (num_elements_in_last_populated == 1)
            {
                --_end_populated_inner;
                _e_size = *(_end_populated_inner - 1) + inner_size;
            }

            // otherwise, just decrement _e_size by one
            else
            {
                --_e_size;
            }
            assert(valid());
            return a;
        }
    }

    // -----
    // front
    // -----

    /**
     * @return a reference to the first element in the container
     */
    reference front()
    {
        // guard against the case where the user calls the default constructor to make the deque
        // in which case, some instance variables are undefined and would later cause errors
        if (_s == 0)
        {
            my_deque tmp(0);
            swap(tmp);
        }
        assert(valid());
        return *(_b_size);
    }

    /**
     * @return a const_reference to the first element in the container
     */
    const_reference front() const
    {
        assert(valid());
        return const_cast<my_deque *>(this)->front();
    }

    // ------
    // insert
    // ------

    /**
     * @param an iterator, a const_reference
     * @brief inserts a value (the reference passed in) at where the iterator is pointing at
     * @return an iterator pointing to the inserted value
     */
    iterator insert(iterator a, const_reference &v)
    {
        size_type idx = a.getIndex();
        if (idx == 0)
        {
            // time complexity: amortized constant
            push_front(v);
        }
        else if (idx == _s)
        {
            // time complexity: amortized constant
            push_back(v);
        }

        // time complexity: linear
        else
        {
            // resize to ensure space
            resize(_s + 1);
            size_type tmp = _s - 1;

            // shift all elements on the right of idx to the right by one
            // to make space for the "v"
            while (tmp > idx)
            {
                (*this)[tmp] = (*this)[tmp - 1];
                --tmp;
            }
            // place the "v"
            (*this)[idx] = v;
        }
        assert(valid());
        return a;
    }

    // ---
    // pop
    // ---

    /**
     * @brief removes the last element of the container
     * calling pop_back on an empty container is undefined
     */
    void pop_back()
    {
        // guard against the case where the user calls the default constructor to make the deque
        // in which case, some instance variables are undefined and would later cause errors
        if (_s == 0)
        {
            my_deque tmp(0);
            swap(tmp);
            assert(valid());
            // undefined
            return;
        }
        // see the _s > s case of resize, which has constant time complexity
        resize(_s - 1);
        assert(valid());
    }

    /**
     * @brief removes the first element of the container
     * calling pop_front on an empty container is undefined
     */
    void pop_front()
    {
        // guard against the case where the user calls the default constructor to make the deque
        // in which case, some instance variables are undefined and would later cause errors
        if (_s == 0)
        {
            my_deque tmp(0);
            swap(tmp);
            assert(valid());
            // undefined
            return;
        }
        ++_b_size;

        // if the element popped is the last element in the first inner array
        // we have to update _b_size to point at the start of the next inner array (the next T)
        // we have to update _begin_populated_inner as well
        // constant time complexity
        if (_b_size == *_begin_populated_inner + inner_size)
        {
            ++_begin_populated_inner;
            _b_size = *_begin_populated_inner;
        }
        --_s;
        assert(valid());
    }

    // ----
    // push
    // ----

    /**
     * @param a const_reference (of a value)
     * @brief appends the given value to the end of the container
     * the new element is initialized as a copy of the const_reference
     */
    void push_back(const_reference &v)
    {
        // guard against the case where the user calls the default constructor
        // in which case, iterators are undefined and resize would cause errors
        if (_s == 0)
        {
            my_deque tmp(0);
            swap(tmp);
        }
        // adding to the end is amortized constant, see the _s < s case of resize
        resize(_s + 1, v);

        assert(valid());
    }

    /**
     * @param a const_reference (of a value)
     * @brief prepends the given value to the beginning of the container
     * the new element is initialized as a copy of the const_reference
     */
    void push_front(const_reference &v)
    {
        if (_s == 0)
        {
            my_deque tmp(0);
            swap(tmp);
        }
        // push_front is equivalent to push_back when deque is empty
        if (_s == 0)
        {
            push_back(v);
            assert(valid());
            return;
        }

        size_type num_elements_in_first_populated = (*_begin_populated_inner + inner_size) - _b_size;

        // there is empty spots in the first populated inner array for me to place the "v"
        // constant time complexity in this if statement
        if (num_elements_in_first_populated < inner_size)
        {
            --_b_size;
            *(_b_size) = v;
        }

        // there isn't empty spots in the first populated inner for me to place the "v"
        // have to allocate a new inner array at the front
        // the loop (copy) inside this else is the reason why push_front is amortized constant
        else
        {
            T **old_begin_populated_inner = _begin_populated_inner;
            T **old_end_populated_inner = _end_populated_inner;

            size_type num_inner = _end_populated_inner - _begin_populated_inner + 1;

            // allocate new outer array
            _begin_populated_inner = _a_outer.allocate(num_inner);
            _end_populated_inner = _begin_populated_inner + num_inner;

            // place the "v" at the end of the first inner array of the new outer array
            *_begin_populated_inner = _a_inner.allocate(inner_size);
            _b_size = *_begin_populated_inner + inner_size - 1;
            *(_b_size) = v;

            // copy over addresses to the inner arrays from the old outer array to the new outer array
            my_uninitialized_copy(_a_outer, old_begin_populated_inner, old_end_populated_inner, _begin_populated_inner + 1);
        }
        ++_s;
        assert(valid());
    }

    // ------
    // resize
    // ------

    /**
     * @brief resize the container to contain s elements
     * @param an "s" of size_type
     * if current size > s, the container is reduced to its first s elements;
     * if current size = s, do nothing;
     * if current size < s, additional default-inserted values are appended
     */
    void resize(size_type s)
    {
        resize(s, 0);
        assert(valid());
    }

    /**
     * @brief resize the container to contain s elements
     * @param an "s" of size_type, a "v" of const_reference
     * if current size > s, the container is reduced to its first s elements;
     * if current size = s, do nothing;
     * if current size < s, additional elements are appended and initialized with copies of v
     */
    void resize(size_type s, const_reference v)
    {
        if (_s == 0)
        {
            my_deque tmp(0);
            swap(tmp);
        }
        if (_s == s)
        {
            assert(valid());
            return;
        }
        size_type num_elements_in_last_populated = _e_size - *(_end_populated_inner - 1);

        // we need to change _s, we need to move _e_size and _end_populated_inner backwards
        if (_s > s)
        {
            // separate out zero case
            if (s == 0)
            {
                my_deque tmp(0);
                swap(tmp);
                assert(valid());
                return;
            }

            // we need to change _s, we need to move _e_size and _end_populated_inner backwards
            if (_s - s < num_elements_in_last_populated)
            {
                // no need to move _end_populated_inner in this case
                // pointer modifications are within the last populated inner array
                _e_size -= (_s - s);
                _s = s;
                assert(valid());
                return;
            }

            // move the ptr backwards by one to skip the last populated array
            --_end_populated_inner;
            _s -= num_elements_in_last_populated;

            // move the ptr backwards, skip a number of inner arrays
            // alter _s at the same time to move it closer to s
            while (_s - s >= inner_size)
            {
                --_end_populated_inner;
                _s -= inner_size;
            }

            // _s - s < inner_size, (*(_end_populated_inner - 1)) is the beginning of
            // [the inner array where the new _e_size is pointing at]
            _e_size = (*(_end_populated_inner - 1)) + inner_size - (_s - s);
            _s = s;
            assert(valid());
            return;
        }

        // _s < s
        else
        {
            if (inner_size - num_elements_in_last_populated >= s - _s)
            {
                // pointer modifications are within the last populated inner array
                // fill (s - _s) number of spots
                my_uninitialized_fill(_a_inner, _e_size, _e_size + (s - _s), v);
                _e_size += s - _s;
                _s = s;
                assert(valid());
                return;
            }
            // here, _s < s and there isn't enough room in the last populated array for (s - _s)

            // if last populated array isn't full, fill these empty spots first and increase _s correspondingly
            if (inner_size - num_elements_in_last_populated != 0)
            {
                my_uninitialized_fill(_a_inner, _e_size, (*(_end_populated_inner - 1)) + inner_size, v);
                _s += inner_size - num_elements_in_last_populated;
            }

            // need more inner arrays
            // the number of inner arrays in the new outer array is the sum of:
            // - num_inner: the number of inner array addresses needed to copy over from the old outer array
            // - num_inner_extra: the number of extra inner arrays needed to hold the "v"s
            size_type num_inner = _end_populated_inner - _begin_populated_inner;
            size_type num_inner_extra = 0;
            if ((s - _s) / inner_size == 0)
            {
                num_inner_extra = 1;
            }
            else if ((s - _s) % inner_size == 0)
            {
                num_inner_extra = (s - _s) / inner_size;
            }
            else
            {
                num_inner_extra = (s - _s) / inner_size + 1;
            }
            size_type num_ptrs_to_populated_inner = num_inner + num_inner_extra;

            // store a pointer to the old outer array so we could copy over addresses of
            // inner arrays residing in those outer arrays
            T **old_begin_populated_inner = _begin_populated_inner;
            T **old_end_populated_inner = _end_populated_inner;

            _begin_populated_inner = _a_outer.allocate(num_ptrs_to_populated_inner);
            _end_populated_inner = _begin_populated_inner + num_ptrs_to_populated_inner;

            // copy over addresses of inner arrays from the old array
            my_uninitialized_copy(_a_outer, old_begin_populated_inner, old_end_populated_inner, _begin_populated_inner);
            // build new inner arrays at the end and put in "v"s
            T **tmp = _begin_populated_inner + num_inner;
            while (tmp != _end_populated_inner)
            {
                // allocate new inner arrays and put the address in the new outer array
                *tmp = _a_inner.allocate(inner_size);
                if (inner_size <= s - _s)
                {
                    my_uninitialized_fill(_a_inner, *tmp, *tmp + inner_size, v);
                    _e_size = (*tmp) + inner_size;
                    _s += inner_size;
                }
                else
                {
                    // have to be the last inner array here, since it could be partial fill, (s - _s) instead of inner_size
                    my_uninitialized_fill(_a_inner, *tmp, *tmp + (s - _s), v);
                    _e_size = (*tmp) + (s - _s);
                    _s = s;
                }
                ++tmp;
            }
            assert(valid());
        }
    }

    // ----
    // size
    // ----

    /**
     * @return the number of elements in the container
     */
    size_type size() const
    {
        assert(valid());
        return _s;
    }

    // ----
    // swap
    // ----

    /**
     * @param a reference to a my_deque
     * https://en.cppreference.com/w/cpp/container/deque/swap
     * @brief exchanges the contents of the container with those of other
     * does not invoke any move, copy, or swap operations on individual elements
     */
    void swap(my_deque &other)
    {
        std::swap(_a_inner, other._a_inner);
        std::swap(_a_outer, other._a_outer);
        std::swap(_b_size, other._b_size);
        std::swap(_e_size, other._e_size);

        std::swap(_begin_populated_inner, other._begin_populated_inner);
        std::swap(_end_populated_inner, other._end_populated_inner);
        std::swap(_s, other._s);
        std::swap(inner_size, other.inner_size);
        assert(valid());
    }
};

#endif // Deque_h

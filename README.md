# CS371g: Generic Programming Deque Repo

* Name: Yin Deng Shuyan Li

* EID: yd4459 sl45644

* GitLab ID: ydeng2012 shuyan_99

* Git SHA: 361c5ea63aab9ca3b196c5b14ba87a12770c1045

* GitLab Pipelines: https://gitlab.com/shuyan_99/cs371g-deque/-/pipelines

* Estimated completion time: 20

* Actual completion time: 30

* Comments: Our code protects iterator integrity when push_back or pop_back, but not for push_front or pop_front, we need more data sharing between my_deque and iterator for that. (See Piazza@165 Invalidation Problem)
